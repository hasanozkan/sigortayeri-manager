//
//  RemoteProcedureProxy.swift
//  ebsswift
//
//  Created by Sigortayeri BT on 08/12/15.
//  Copyright © 2015 Sigortayeri. All rights reserved.
//

import Foundation

typealias RPPCallbackBlockType = (NSDictionary?, NSError?) -> Void

class RemoteProcedureProxy {
    var EndPoint = "https://mobilemw.sigortayeri.com/Service.asmx";
    var apiKey = "O84XN6n9E2h6A9j27dn403m43a25O224m8yf95yC64GO845ni20F54Q7zm49e407XPWH26p406gHyYf36SZA6M04r02CWP89425Qg4J3895g42A6rb06994m4y0c1zdu"
    var appSessionID:String = "";
    var userSessionID:String {
        set {
            let standardUserDefaults = NSUserDefaults.standardUserDefaults()
            standardUserDefaults.setObject(newValue, forKey: "userSessionID")
            standardUserDefaults.synchronize()
        }
        get {
            if let userSessionID = NSUserDefaults.standardUserDefaults().stringForKey("userSessionID") {
                return userSessionID
            }
            return ""
        }
    }
    var username:String {
        set {
            let standardUserDefaults = NSUserDefaults.standardUserDefaults()
            standardUserDefaults.setObject(newValue, forKey: "username")
            standardUserDefaults.synchronize()
        }
        get {
            if let username = NSUserDefaults.standardUserDefaults().stringForKey("username") {
                return username
            }
            return ""
        }
    }
    
    func getSessionID(callback:RPPCallbackBlockType){
        let manager = AFHTTPRequestOperationManager()
        manager.requestSerializer.setValue(self.apiKey, forHTTPHeaderField: "apiKey")
        
        let parameters = ["userName":"MOBILAPP","password":"OQYjOXD0u/HG5TJqkm4Tjw=="]
        manager.POST(self.EndPoint + "/Login",
            parameters: parameters,
            success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                if(responseObject == nil || !responseObject.isKindOfClass(NSDictionary)){
                    callback(nil,NSError(domain: "com.sigortayeri.ebsswift", code: 11, userInfo: nil))
                    return;
                }
             
                let rootObject = responseObject.objectForKey("RootObject");
                if(rootObject == nil || !rootObject!.isKindOfClass(NSDictionary)){
                    callback(nil,NSError(domain: "com.sigortayeri.ebsswift", code: 11, userInfo: nil))
                    return;
                }
                self.appSessionID = rootObject?.objectForKey("appSessionID") as! String;
                callback(responseObject as? NSDictionary,nil)
            },
            failure:{ (operation: AFHTTPRequestOperation?,error: NSError!) in
                callback(nil,error)
            }
            
        )
    }
    
    func dispatchPOST(URLString:String, parameters: NSDictionary?, callback:RPPCallbackBlockType){
        getSessionID { (responseObject, error) -> Void in
        if(error == nil){
            let manager = AFHTTPRequestOperationManager()
            manager.requestSerializer.setValue(self.apiKey, forHTTPHeaderField: "apiKey")
            manager.requestSerializer.setValue(self.appSessionID, forHTTPHeaderField: "appSessionID")
            
            if(self.userSessionID.characters.count>0){
                manager.requestSerializer.setValue(self.userSessionID, forHTTPHeaderField: "userSessionID")
            }
            
            manager.POST(self.EndPoint + "/" + URLString,
                parameters: parameters,
                success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                    if(responseObject != nil && responseObject!.isKindOfClass(NSDictionary)){
                        if(responseObject?.valueForKey("Status")?.stringValue == "1"){
                            callback(nil,NSError(domain: "com.sigortayeri.ebsswift", code: 11, userInfo: nil))
                        }else{
                            callback(responseObject as? NSDictionary,nil)
                        }
                    }else{
                        callback(nil,error)
                    }
                },
                failure:{ (operation: AFHTTPRequestOperation?,error: NSError!) in
                    callback(nil,error)
                }
            )
        }else{
            callback(nil,error)
            }
        }
    }
    
    func Check_UserPassApp(username:String, password:String, applicationcode:String,callback:RPPCallbackBlockType) -> Void{
        let paramaters = ["username":username, "password":password, "applicationcode":applicationcode] as NSDictionary
        dispatchPOST("Check_UserPassApp", parameters: paramaters)
            { (response, error) -> Void in
                if(error == nil){
                    let status = response?.objectForKey("Status")
                    if(status != nil && status?.stringValue == "0"){
                        let rootObject = response!.objectForKey("RootObject");
                        if(rootObject is NSDictionary){
                            let sessionID = rootObject?.objectForKey("userSessionID")
                            if(sessionID is NSString){
                                self.userSessionID = sessionID as! String
                                self.username = username
                                callback(response,nil)
                            }else{
                                callback(response,NSError(domain: "com.sigortayeri.ebsswift", code: 11, userInfo: nil))
                            }
                            
                        }else{
                            callback(response,NSError(domain: "com.sigortayeri.ebsswift", code: 11, userInfo: nil))
                        }
                    }else{
                        callback(response,error)
                    }
                }else{
                    callback(response,error)
                }
        }
    }
    
    func getDashboardProductionReport(startDate:String, endDate:String, type:String, application:String,callback:RPPCallbackBlockType) -> Void{
        dispatchPOST("GET_DASHBOARD_PRODUCTION_REPORT", parameters: ["USERNAME":self.username, "START_DATE":startDate, "END_DATE":endDate, "TYPE":type, "APPLICATION":application]){ (responseObject, error) -> Void in
                callback(responseObject, error)
        }
    }
}
