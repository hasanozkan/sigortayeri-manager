//
//  LoginViewController.swift
//  ebsswift
//
//  Created by Sigortayeri BT on 08/12/15.
//  Copyright © 2015 Sigortayeri. All rights reserved.
//

import Foundation

class LoginViewController: UIViewController,UITextFieldDelegate {
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismiss")
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func dismiss() {
        self.view.endEditing(true)
    }
    
    @IBAction func btnLoginAction(sender: UIButton) {
        let waitAlert = UIAlertView(title: "Lütfen bekleyin", message: nil, delegate: nil, cancelButtonTitle: nil)
        waitAlert.show()
        let service = RemoteProcedureProxy()
        service.Check_UserPassApp(userNameTextField.text!, password: passwordTextField.text!,applicationcode: "S-BOARD") { (response, error) -> Void in
            waitAlert.dismissWithClickedButtonIndex(0, animated: true)
            if (error == nil) {
                if(response?.objectForKey("Status")?.stringValue == "1"){
                    let message = response?.objectForKey("Message") as? String
                    if #available(iOS 8.0, *) {
                        let alertController = UIAlertController(title: "Uyarı!", message: message, preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.Default,handler: nil))
                        self.presentViewController(alertController, animated: true, completion: nil)
                    } else {
                       let alert = UIAlertView(title: "Uyarı!", message: message, delegate: nil, cancelButtonTitle: "Tamam")
                        alert.show()
                    }
                    
                }else {
                    self.appDelegate.window!.rootViewController = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateInitialViewController()
                }
            }else{
                if #available(iOS 8.0, *) {
                    let alertController = UIAlertController(title: "Uyarı!", message: "İşlem başarısız oldu.", preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.Default,handler: nil))
                    self.presentViewController(alertController, animated: true, completion: nil)
                } else {
                    let alert = UIAlertView(title: "Uyarı!", message: "İşlem başarısız oldu.", delegate: nil, cancelButtonTitle: "Tamam")
                    alert.show()
                }
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField == userNameTextField){
            passwordTextField.becomeFirstResponder()
        }else if(textField == passwordTextField){
            self.view.endEditing(true)
        }
        return true
    }
}
