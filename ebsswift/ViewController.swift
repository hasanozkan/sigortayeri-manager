//
//  ViewController.swift
//  ebsswift
//
//  Created by Sigortayeri BT on 08/12/15.
//  Copyright © 2015 Sigortayeri. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var pickOption = ["Bugün", "Bu ay", "Geçen ay", "Bu yıl", "Geçen yıl"]
    var pickerView:UIPickerView?
    var refreshControl:UIRefreshControl!
    var activityIndicator:UIActivityIndicatorView?
    var viewDefaulCenter:CGPoint!
    var pullStart = false
    
    
    @IBOutlet weak var lblPrim: UILabel!
    @IBOutlet weak var lblKomisyon: UILabel!
    @IBOutlet weak var lblAdet: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceView2: UIView!
    @IBOutlet weak var startDate: UITextField!
    @IBOutlet weak var endDate: UITextField!
    @IBOutlet weak var dateRangePickerTextFiled: UITextField!
    @IBOutlet weak var dateRangeButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var dateRangePicket: UIPickerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismiss")
        view.addGestureRecognizer(tap)
        let date = NSDate()
        appDelegate.startDate =  Tools.getDateFormatter().stringFromDate(date)
        appDelegate.endDate = Tools.getDateFormatter().stringFromDate(date)
        appDelegate.dateRange = dateRangePickerTextFiled.text
        
        pickerView = UIPickerView()
        pickerView!.delegate = self
        dateRangePickerTextFiled.inputView = pickerView
        dateRangePickerTextFiled.tintColor = UIColor.clearColor()
        startDate.tintColor = UIColor.clearColor()
        endDate.tintColor = UIColor.clearColor()
        
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: "handlePan:")
        self.containerView.addGestureRecognizer(gestureRecognizer)
        
        
        activityIndicator = UIActivityIndicatorView (frame: CGRectMake(0,0, 80, 80)) as UIActivityIndicatorView
        activityIndicator!.center = CGPointMake(self.view.center.x, self.containerView.frame.origin.y + 20)
        activityIndicator!.hidesWhenStopped = true
        activityIndicator!.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(activityIndicator!)
    }
    
    @IBAction func handlePan(gestureRecognizer: UIPanGestureRecognizer) {
        if (gestureRecognizer.state == UIGestureRecognizerState.Began || gestureRecognizer.state == UIGestureRecognizerState.Changed) {
            let translation = gestureRecognizer.translationInView(self.containerView)
            if(translation.y < 5 || translation.y > 30){
                return
            }
            
            activityIndicator!.startAnimating()
            if(!pullStart){
                viewDefaulCenter = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y )
                pullStart = true;
                reloadData()
            }
            gestureRecognizer.view!.center = CGPointMake(gestureRecognizer.view!.center.x, gestureRecognizer.view!.center.y + translation.y)
            gestureRecognizer.setTranslation(CGPointMake(0,0), inView: self.containerView)
        }
    }
    
    func refresh(sender:AnyObject)
    {
        reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        startDate.text =  appDelegate.startDate
        endDate.text = appDelegate.endDate
        dateRangePickerTextFiled.text = appDelegate.dateRange
        pickerView?.selectRow(pickOption.indexOf(appDelegate.dateRange!)!, inComponent: 0, animated: true)
        self.reloadData()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        activityIndicator!.center = CGPointMake(self.view.center.x, self.containerView.frame.origin.y + 20)
    }
    
    func reloadData(){
            let formatter = NSNumberFormatter()
            formatter.numberStyle = .CurrencyStyle
            formatter.locale = NSLocale(localeIdentifier: "tr_TR")
            formatter.currencySymbol = ""
            let service = RemoteProcedureProxy()
            service.getDashboardProductionReport(self.startDate.text!, endDate: self.endDate.text!, type: "SUMMARY", application: "",callback: { (response, error) -> Void in
                if(self.pullStart){
                    let delayInSeconds = 1.0;
                    let popTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delayInSeconds * Double(NSEC_PER_SEC)));
                    dispatch_after(popTime, dispatch_get_main_queue()) { () -> Void in
                        self.containerView!.center = self.viewDefaulCenter
                        self.pullStart = false
                        self.activityIndicator?.stopAnimating()
                    }
                }
                if(error != nil){
                    return
                }
                
                let rootObj = response?.objectForKey("RootObject") as? NSArray
                if(rootObj == nil || rootObj?.count==0 ){
                    return;
                }
                if let item = rootObj?.objectAtIndex(0){
                    if(item.valueForKey("ADET") is NSNull) {
                        self.lblAdet.text = "0"
                    }else{
                        self.lblAdet.text = item.valueForKey("ADET") as? String
                    }
                    if(item.valueForKey("KOMISYON") is NSNull) {
                        self.lblKomisyon.text = "0"
                    }else {
                        self.lblKomisyon.text = item.valueForKey("KOMISYON") as? String
                    }
                    if(item.valueForKey("PRIM") is NSNull) {
                        self.lblPrim.text = "0";
                    }else{
                        self.lblPrim.text = item.valueForKey("PRIM") as? String
                    }
                }
            })
    }
    
    @IBAction func dismiss() {
        self.view.endEditing(true)
    }
    @IBAction func textFieldEditing(sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.date = Tools.getDateFormatter().dateFromString(startDate.text!)!
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: Selector("startDatePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    @IBAction func endDateFieldEditing(sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.date = Tools.getDateFormatter().dateFromString(endDate.text!)!
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: Selector("endDatePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func startDatePickerValueChanged(sender:UIDatePicker) {
        appDelegate.startDate = Tools.getDateFormatter().stringFromDate(sender.date)
        startDate.text = appDelegate.startDate
        reloadData()
    }
    
    func endDatePickerValueChanged(sender:UIDatePicker) {
        appDelegate.endDate = Tools.getDateFormatter().stringFromDate(sender.date)
        endDate.text = appDelegate.endDate
        reloadData()
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let date = NSDate()
        
        if(pickOption[row]=="Bugün"){
            startDate.text =  Tools.getDateFormatter().stringFromDate(date)
            endDate.text = Tools.getDateFormatter().stringFromDate(date)
        }else if(pickOption[row]=="Bu ay"){
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Year, .Month], fromDate: date)
            let startOfMonth = calendar.dateFromComponents(components)!
            startDate.text =  Tools.getDateFormatter().stringFromDate(startOfMonth)
            
            let comps2 = NSDateComponents()
            comps2.month = 1
            comps2.day = -1
            let endOfMonth = calendar.dateByAddingComponents(comps2, toDate: startOfMonth, options: [])!
            
            endDate.text = Tools.getDateFormatter().stringFromDate(endOfMonth)
        }else if(pickOption[row]=="Bu yıl"){
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Year], fromDate: date)
            let startOfMonth = calendar.dateFromComponents(components)!
            startDate.text =  Tools.getDateFormatter().stringFromDate(startOfMonth)
            endDate.text = Tools.getDateFormatter().stringFromDate(date)
        }else if(pickOption[row]=="Geçen ay"){
            let calendar = NSCalendar.currentCalendar()
            let comps2 = NSDateComponents()
            comps2.month = -1
            comps2.day = 0
            let components = calendar.components([.Year, .Month], fromDate: date)
            let startOfMonth = calendar.dateFromComponents(components)!
            let startLastOfMonth = calendar.dateByAddingComponents(comps2, toDate: startOfMonth, options: [])!
            startDate.text =  Tools.getDateFormatter().stringFromDate(startLastOfMonth)
            
            comps2.month = 1
            comps2.day = -1
            let endOfMonth = calendar.dateByAddingComponents(comps2, toDate: startLastOfMonth, options: [])!
            endDate.text = Tools.getDateFormatter().stringFromDate(endOfMonth)
        }else if(pickOption[row]=="Geçen yıl"){
            let calendar = NSCalendar.currentCalendar()
            let comps2 = NSDateComponents()
            
            let components = calendar.components([.Year], fromDate: date)
            let startOfYear = calendar.dateFromComponents(components)!
            comps2.year = -1
            let startLastOfYear = calendar.dateByAddingComponents(comps2, toDate: startOfYear, options: [])!
            startDate.text =  Tools.getDateFormatter().stringFromDate(startLastOfYear)
            
            comps2.year = 1
            comps2.day = -1
            let endOfYear = calendar.dateByAddingComponents(comps2, toDate: startLastOfYear, options: [])!
            endDate.text = Tools.getDateFormatter().stringFromDate(endOfYear)
        }
        reloadData()
        dateRangePickerTextFiled.text = pickOption[row]
        
        appDelegate.startDate =  startDate.text
        appDelegate.endDate = endDate.text
        appDelegate.dateRange = dateRangePickerTextFiled.text
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.blackColor()
        pickerLabel.text = pickOption[row]
        // pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 15)
        pickerLabel.font = UIFont(name: "Arial-BoldMT", size: 15) // In this use your custom font
        pickerLabel.textAlignment = NSTextAlignment.Center
        return pickerLabel
    }
    
    @IBAction func dateRangePickerClick(sender: UIButton) {
        dateRangePickerTextFiled.becomeFirstResponder()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

