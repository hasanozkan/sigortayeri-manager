//
//  SalesTableViewCell.swift
//  ebsswift
//
//  Created by Sigortayeri BT on 09/12/15.
//  Copyright © 2015 Sigortayeri. All rights reserved.
//

import Foundation


class SalesTableCell:MGSwipeTableCell {
    @IBOutlet weak var kanalLabel: UILabel!
    @IBOutlet weak var adetLabel: UILabel!
    @IBOutlet weak var primLabel: UILabel!
    @IBOutlet weak var komisyonLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}