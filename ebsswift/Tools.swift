//
//  Tools.swift
//  ebsswift
//
//  Created by Sigortayeri BT on 10/12/15.
//  Copyright © 2015 Sigortayeri. All rights reserved.
//

import Foundation

class Tools {
    static func getDateFormatter()->NSDateFormatter{
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        formatter.timeStyle = .NoStyle
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter
    }
}
