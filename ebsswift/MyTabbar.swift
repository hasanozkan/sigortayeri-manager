//
//  MyTabbar.swift
//  ebsswift
//
//  Created by Sigortayeri BT on 08/12/15.
//  Copyright © 2015 Sigortayeri. All rights reserved.
//

import Foundation

class MyTabbar:UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.tabBar.hidden = true;
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        navigationController!.navigationBar.translucent = false
        navigationController!.navigationBar.barTintColor = UIColor.whiteColor()
        navigationController?.navigationBar.tintColor = UIColor(red: 22.0/255.0, green: 125.0/255.0, blue: 18.0/255.0, alpha: 1.0)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Geri", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        
        UITabBar.appearance().tintColor = UIColor.blackColor();
        UITabBar.appearance().selectedImageTintColor = UIColor.whiteColor();
        
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.whiteColor()], forState:.Selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.whiteColor()], forState:.Normal)
    }
    
    @IBAction func btnLogoutClicj(sender: UIButton) {
        let standardUserDefaults = NSUserDefaults.standardUserDefaults()
        standardUserDefaults.removeObjectForKey("userSessionID")
        standardUserDefaults.removeObjectForKey("username")
        standardUserDefaults.synchronize()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let rootViewController = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("LoginView")
        appDelegate.window!.rootViewController = rootViewController;
    }
    @IBAction func switchValueDidChange(sender: DGRunkeeperSwitch!) {
        selectedIndex = sender.selectedIndex
    }
    
}
