//
//  DetailViewController.swift
//  ebsswift
//
//  Created by Sigortayeri BT on 08/12/15.
//  Copyright © 2015 Sigortayeri. All rights reserved.
//
import UIKit



class DetailViewController: UIViewController,UITableViewDelegate ,UIPickerViewDataSource,UIPickerViewDelegate{
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var salesData:NSArray = [];
    var pickOption = ["Bugün", "Bu ay", "Geçen ay", "Bu yıl", "Geçen yıl"]
    var pickerView:UIPickerView?
    var refreshControl:UIRefreshControl!
    
    @IBOutlet weak var salesTable: UITableView!
    @IBOutlet weak var editTextStartDate: UITextField!
    @IBOutlet weak var editTextEndDate: UITextField!
    @IBOutlet weak var dateRangePickerTextFiled: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismiss")
        view.addGestureRecognizer(tap)
        
        pickerView = UIPickerView()
        pickerView!.delegate = self
        dateRangePickerTextFiled.inputView = pickerView
        dateRangePickerTextFiled.tintColor = UIColor.clearColor()
        editTextStartDate.tintColor = UIColor.clearColor()
        editTextEndDate.tintColor = UIColor.clearColor()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.backgroundColor = UIColor(red: 235.0/255.0, green: 253.0/255.0, blue: 228.0/255.0, alpha: 1.0)
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        self.salesTable.addSubview(refreshControl)
        
    }
    
    func refresh(sender:AnyObject)
    {
        reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        editTextStartDate.text =  appDelegate.startDate
        editTextEndDate.text = appDelegate.endDate
        dateRangePickerTextFiled.text = appDelegate.dateRange
        pickerView?.selectRow(pickOption.indexOf(appDelegate.dateRange!)!, inComponent: 0, animated: true)
        self.reloadData()
    }
    
    func reloadData(){
        let service = RemoteProcedureProxy()
        service.getDashboardProductionReport(self.editTextStartDate.text!, endDate: self.editTextEndDate.text!, type: "APPLICATION",application: "",callback: { (response, error) -> Void in
            self.refreshControl.endRefreshing()
            if(error != nil){
                return
            }
            self.salesData = response?.objectForKey("RootObject") as! NSArray
            self.salesTable.reloadData()
            
        })

    }
    
    @IBAction func dismiss() {
        self.view.endEditing(true)
    }
    
    @IBAction func startDateBeginEdit(sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.date = Tools.getDateFormatter().dateFromString(editTextStartDate.text!)!
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: Selector("startDatePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    @IBAction func endDateBeginEdit(sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.date = Tools.getDateFormatter().dateFromString(editTextEndDate.text!)!
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: Selector("endDatePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func startDatePickerValueChanged(sender:UIDatePicker) {
        appDelegate.startDate = Tools.getDateFormatter().stringFromDate(sender.date)
        editTextStartDate.text = appDelegate.startDate
        reloadData()
    }
    
    func endDatePickerValueChanged(sender:UIDatePicker) {
        appDelegate.endDate = Tools.getDateFormatter().stringFromDate(sender.date)
        editTextEndDate.text = appDelegate.endDate
        reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return salesData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as? SalesTableCell
        let rowData = salesData.objectAtIndex(indexPath.row) as? NSDictionary
        if(rowData != nil)
        {
            let formatter = NSNumberFormatter()
            formatter.numberStyle = .CurrencyStyle
            formatter.locale = NSLocale(localeIdentifier: "tr_TR")
            formatter.currencySymbol = ""
            
            cell?.kanalLabel.text = rowData!.valueForKey("KANAL") as? String
            cell?.adetLabel.text = rowData!.valueForKey("ADET") as? String
            cell?.primLabel.text = rowData!.valueForKey("PRIM") as? String
            cell?.komisyonLabel.text = rowData!.valueForKey("KOMISYON") as? String
            
            let altkanal = MGSwipeButton(title: "Alt Kanal", backgroundColor: UIColor(red: 22.0/255.0, green: 125.0/255.0, blue: 18.0/255.0, alpha: 1.0),callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                let applicationCode = rowData!.valueForKey("KANALKODU") as? String
                let subApplicationView = self.storyboard!.instantiateViewControllerWithIdentifier("subApplicationView") as! SubApplicationViewController
                subApplicationView.applicationCode = applicationCode
                subApplicationView.title = rowData!.valueForKey("KANAL") as? String
                self.navigationController!.pushViewController(subApplicationView, animated: true)
                return true
            })

            let grafik = MGSwipeButton(title: "Grafik", backgroundColor: UIColor.orangeColor(),callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                print("Convenience callback for swipe buttons!")
                return true
            })
            
            cell?.rightButtons = [grafik,altkanal]
     

        }
        return cell!
    }
        
    /*@available(iOS 8.0, *)
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let rowData = salesData.objectAtIndex(indexPath.row) as? NSDictionary
        
        let more = UITableViewRowAction(style: .Normal, title: "Alt Kanal") { action, index in
            tableView.setEditing(false, animated: true)
            let applicationCode = rowData!.valueForKey("KANALKODU") as? String
            let subApplicationView = self.storyboard!.instantiateViewControllerWithIdentifier("subApplicationView") as! SubApplicationViewController
            subApplicationView.applicationCode = applicationCode
            subApplicationView.title = rowData!.valueForKey("KANAL") as? String
            self.navigationController!.pushViewController(subApplicationView, animated: true)
        }
        more.backgroundColor = UIColor(red: 22.0/255.0, green: 125.0/255.0, blue: 18.0/255.0, alpha: 1.0)
        
        let favorite = UITableViewRowAction(style: .Normal, title: "Grafik") { action, index in
            print("favorite button tapped")
        }
        favorite.backgroundColor = UIColor.orangeColor()
        
        return [favorite, more]
    }*/
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let date = NSDate()
        if(pickOption[row]=="Bugün"){
            editTextStartDate.text =  Tools.getDateFormatter().stringFromDate(date)
            editTextEndDate.text = Tools.getDateFormatter().stringFromDate(date)
        }else if(pickOption[row]=="Bu ay"){
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Year, .Month], fromDate: date)
            let startOfMonth = calendar.dateFromComponents(components)!
            editTextStartDate.text =  Tools.getDateFormatter().stringFromDate(startOfMonth)
            
            let comps2 = NSDateComponents()
            comps2.month = 1
            comps2.day = -1
            let endOfMonth = calendar.dateByAddingComponents(comps2, toDate: startOfMonth, options: [])!
            
            editTextEndDate.text = Tools.getDateFormatter().stringFromDate(endOfMonth)
        }else if(pickOption[row]=="Bu yıl"){
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Year], fromDate: date)
            let startOfMonth = calendar.dateFromComponents(components)!
            editTextStartDate.text =  Tools.getDateFormatter().stringFromDate(startOfMonth)
            editTextEndDate.text = Tools.getDateFormatter().stringFromDate(date)
        }else if(pickOption[row]=="Geçen ay"){
            let calendar = NSCalendar.currentCalendar()
            let comps2 = NSDateComponents()
            comps2.month = -1
            comps2.day = 0
            let components = calendar.components([.Year, .Month], fromDate: date)
            let startOfMonth = calendar.dateFromComponents(components)!
            let startLastOfMonth = calendar.dateByAddingComponents(comps2, toDate: startOfMonth, options: [])!
            editTextStartDate.text =  Tools.getDateFormatter().stringFromDate(startLastOfMonth)
            
            comps2.month = 1
            comps2.day = -1
            let endOfMonth = calendar.dateByAddingComponents(comps2, toDate: startLastOfMonth, options: [])!
            editTextEndDate.text = Tools.getDateFormatter().stringFromDate(endOfMonth)
        }else if(pickOption[row]=="Geçen yıl"){
            let calendar = NSCalendar.currentCalendar()
            let comps2 = NSDateComponents()
            
            let components = calendar.components([.Year], fromDate: date)
            let startOfYear = calendar.dateFromComponents(components)!
            comps2.year = -1
            let startLastOfYear = calendar.dateByAddingComponents(comps2, toDate: startOfYear, options: [])!
            editTextStartDate.text =  Tools.getDateFormatter().stringFromDate(startLastOfYear)
            
            comps2.year = 1
            comps2.day = -1
            let endOfYear = calendar.dateByAddingComponents(comps2, toDate: startLastOfYear, options: [])!
            editTextEndDate.text = Tools.getDateFormatter().stringFromDate(endOfYear)
        }
        reloadData()
        
        dateRangePickerTextFiled.text = pickOption[row]
        appDelegate.startDate = editTextStartDate.text
        appDelegate.endDate = editTextEndDate.text
        appDelegate.dateRange = dateRangePickerTextFiled.text
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.blackColor()
        pickerLabel.text = pickOption[row]
        // pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 15)
        pickerLabel.font = UIFont(name: "Arial-BoldMT", size: 15) // In this use your custom font
        pickerLabel.textAlignment = NSTextAlignment.Center
        return pickerLabel
    }
    
    @IBAction func dateRangePickerClick(sender: UIButton) {
        dateRangePickerTextFiled.becomeFirstResponder()
    }
}
